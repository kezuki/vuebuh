function setNameAndPathRoute(str) {
	return {
		path: '/' + str.toLowerCase(),
		name:str
	}
}

export default setNameAndPathRoute;