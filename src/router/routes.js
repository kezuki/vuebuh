import setNameAndPathRoute from "./utils/setNameAndPathRoute";
const Home = import('@/views/Home');
const Login = import('@/views/Login');
const Register = import('@/views/Register');
const Categories = import('@/views/Categories');
const Record = import('@/views/Record');
const Profile = import('@/views/Profile');
const Planning = import('@/views/Planning');
const History = import('@/views/History');
const Detail = import('@/views/Detail');

const routes = [
	{
		path: '/',
		name: 'home',
		meta: {layout: 'main'},
		component: () => Home
	},
	{
		...setNameAndPathRoute('Login'),
		meta: {layout: 'empty'},
		component:() => Login
	},
	{
		...setNameAndPathRoute('Register'),
		meta: {layout: 'empty'},
		component:() => Register
	},
	{
		...setNameAndPathRoute('Categories'),
		meta: {layout: 'main'},
		component: () => Categories
	},
	{
		...setNameAndPathRoute('Record'),
		meta: {layout: 'main'},
		component: () => Record
	},
	{
		...setNameAndPathRoute('Profile'),
		meta: {layout: 'main'},
		component: () => Profile
	},
	{
		...setNameAndPathRoute('Planning'),
		meta: {layout: 'main'},
		component: () => Planning
	},
	{
		...setNameAndPathRoute('History'),
		meta: {layout: 'main'},
		component: () => History
	},
	{
		...setNameAndPathRoute('Detail'),
		meta: {layout: 'main'},
		component: () => Detail
	}
	
];

export default routes;